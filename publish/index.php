<?php

/**
 * Require autoload file
 */

require_once __DIR__.'/../vendor/autoload.php';

/**
 * Require boot app file
 */

require_once __DIR__.'/../boot/app.php';